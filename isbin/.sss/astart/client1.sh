#!/bin/bash
# 1 сервер 
localip="178.62.246.44"
clientip=""
nextserverip="188.227.18.83"

# Обновляем и ставим необходимые пакеты

apt-get update -y

apt-get install openvpn easy-rsa  tmux atop htop tcpdump iptables-persistent -y

# Вносим правку в sysctl.conf
echo net.ipv4.ip_forward=1 >> /etc/sysctl.conf
sysctl -p 


# Создаем client.conf
echo " create client.conf"

echo client > /etc/openvpn/client.conf
echo dev tun20 >> /etc/openvpn/client.conf
echo proto udp >> /etc/openvpn/client.conf
echo remote $nextserverip >> /etc/openvpn/client.conf
echo resolv-retry infinite >> /etc/openvpn/client.conf
echo nobind >> /etc/openvpn/client.conf
echo persist-key >> /etc/openvpn/client.conf
echo persist-tun >> /etc/openvpn/client.conf
echo mute-replay-warnings >> /etc/openvpn/client.conf
echo ca /etc/openvpn/client/ca.crt >> /etc/openvpn/client.conf
echo cert /etc/openvpn/client/client1.crt >> /etc/openvpn/client.conf
echo key /etc/openvpn/client/client1.key >> /etc/openvpn/client.conf
echo tls-auth /etc/openvpn/client/ta.key 1 >> /etc/openvpn/client.conf
echo script-security 3 system >> /etc/openvpn/client.conf
echo up /etc/openvpn/iprules.sh >> /etc/openvpn/client.conf
echo verb 0 >> /etc/openvpn/client.conf
echo log /dev/null >> /etc/openvpn/client.conf
echo ns-cert-type server >> /etc/openvpn/client.conf
echo comp-lzo >> /etc/openvpn/client.conf
echo verb 0 >> /etc/openvpn/client.conf
echo keepalive 10 120 >> /etc/openvpn/client.conf


# Создаем /etc/openvpn/iprules.sh
echo  "Создаем /etc/openvpn/iprules.sh"


echo '#!/bin/sh' > /etc/openvpn/iprules.sh
echo '' >> /etc/openvpn/iprules.sh
echo  '#ip rule add from 192.168.111.0/16 table vpn.out' >> /etc/openvpn/iprules.sh
echo  '#ip route add default dev tun20 table vpn.out' >> /etc/openvpn/iprules.sh

chmod 777 /etc/openvpn/iprules.sh

# Правим  /etc/iproute2/rt_tables

echo '#' > /etc/iproute2/rt_tables

echo '# reserved values' >> /etc/iproute2/rt_tables
echo '# >>' /etc/iproute2/rt_tables
echo 255     local >> /etc/iproute2/rt_tables
echo 254     main >> /etc/iproute2/rt_tables
echo 253     default >> /etc/iproute2/rt_tables
echo 0       unspec >> /etc/iproute2/rt_tables
echo '#' >> /etc/iproute2/rt_tables
echo '# local' >> /etc/iproute2/rt_tables
echo '#' >> /etc/iproute2/rt_tables
# echo '#1      inr.ruhep' >> /etc/iproute2/rt_tables
echo  150 vpn.out >> /etc/iproute2/rt_tables

# Правим iptables 
echo " CREATE iptables rules" 

echo '# START OPENVPN RULES' > /etc/iptables/rules.v4
echo  '# NAT table rules' >> /etc/iptables/rules.v4
echo  *nat >> /etc/iptables/rules.v4
echo :POSTROUTING ACCEPT [0:0] >> /etc/iptables/rules.v4
echo '# Allow traffic from OpenVPN client to eth0' >> /etc/iptables/rules.v4
echo -A POSTROUTING -s 192.168.111.0/24 -o tun20  -j MASQUERADE >> /etc/iptables/rules.v4
echo COMMIT >> /etc/iptables/rules.v4
echo '# END OPENVPN RULES' >> /etc/iptables/rules.v4
echo *filter >> /etc/iptables/rules.v4
echo :INPUT ACCEPT [0:0] >> /etc/iptables/rules.v4
echo :FORWARD ACCEPT [0:0] >> /etc/iptables/rules.v4
echo :OUTPUT ACCEPT [0:0] >> /etc/iptables/rules.v4
echo COMMIT >> /etc/iptables/rules.v4

echo "restart iptables"
service netfilter-persistent restart

echo "RESTART openvpn"

service openvpn restart 

ifconfig

echo " Если есть интерфейс tun0 & tun20 то openvpn сервер скорее всего поднялся нормально "
bebetter666