#!/bin/bash

exdir="$(cd "$(dirname "$0")" && pwd)/../"

source "${exdir}setting/.main_cfg"

source "${exdir}lib/notif.sl"

tdir=$exdir"tmp/"

arg_parse(){
    while [[ $# -gt 1 ]]
    do
	key="$1"
	case $key in
	    -p|--path)
		ppath="$2"
		shift # past argument
	    ;;
	    -d|--dir)
		dpath=$(readlink -m $2	)
		shift # past argument
	    ;;
	    -f|--file)
		fpath=$(readlink -m $2)
		shift # past argument
	    ;;
	    default)
		DEFAULT=YES
	    ;;
	    *)
        	# unknown option
	    ;;
	esac
	shift # past argument or value
    done
}

setvar(){
    tmpdir=$exdir"tmp/vars"
    if [ ! -f $tmpdir ] 
	then cat /dev/null > "$tmpdir"
    fi
    if [[ ! -z $1 ]] && [[ ! -z $2 ]]
	then tmpv=$( grep -v "$1" $tmpdir)
	tmpv=$(echo "$tmpv" | sed '/^$/d') 
	#echo -e "\n!->$1 : $2 --- $tmpv\n"
	echo "$tmpv" > "$tmpdir"
	echo "${1}=${2}" >> "$tmpdir"
    else
	echo "4"
    fi
}
getvar(){
    if [[ ! -z $1 ]]; then
	tmpdir=$exdir"tmp/vars"
	chk=$(grep "$1" "$tmpdir")
	if [[ ! -z $chk ]]; then
	    tmpdir=$exdir"/tmp/vars"
	    source $tmpdir
	else
	    eval $1=""
	fi
	#echo ${!1}"
    fi
}
function r() {
    number=$1
    shift
    for n in $(seq $number); do
      $@
    done
}
# @1:string @2:delimiter
split(){
	local -a ar=()
	delim=${2:-"e"}
	echo $1
	res=$(echo $1 | tr "$delim" "\n")
	for a in $res
		do ar+=(\"$a\")
	done
	#echo ${ar[1]}
	echo ${ar[@]}
}


##visual
gl(){
	COLUMNS=$(tput cols)
	#echo $COLUMNS
	if [ -z $1 ]
		then l=30
	else 	if [ $1 == "f" ]
			then l=$COLUMNS
			else l=$1
		fi
	fi
	line=$(r $l echo -e "_\c")
	echo -e $line
	br
}
align_c(){
	COLUMNS=$(tput cols)
	title=$1
	printf "%*s\n" $(((${#title}+$COLUMNS)/2)) "$title"
}
title(){
	br
	align_c $1
	align_c $(gl 25)
	align_c $(gl 40)
}
getinfo(){
	declare -a ll="($(split $1))"
	echo -e "  * \c"
	for t in ${ll[@]}
		do echo -e "$t\t\c"
	done
	br
}
br(){
	echo -e ""
}
