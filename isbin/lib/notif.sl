#!/bin/bash
ask() {
    local AMSURE
    if [ -n "$1" ] ; then
	read -n 1 -p "$1 (y/[a]): " AMSURE
    else
	read -n 1 AMSURE
    fi
    echo "" 1>&2
    if [ "$AMSURE" = "y" ] ; then
	return 0
    else
	return 1
    fi
read -n 1 -p "Ты уверен, что хочешь запустить это (y/[a]): " AMSURE 
[ "$AMSURE" = "y" ] || exit
echo "" 1>&2
}