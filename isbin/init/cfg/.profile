# ~/.profile: executed by Bourne-compatible login shells.

if [ "$BASH" ]; then
  if [ -f ~/.bashrc ]; then
    . ~/.bashrc 
  fi
fi
if [ -d "/isbin" ] ; then
  PATH="$PATH:/isbin/init:/isbin/setting:/isbin/system:/isbin/work:/isbin/lib:/isbin/etc"
fi
mesg n